# Altair Flask server and Vue.js client for TOiK classes at AGH

### Setup

Required software:
- Node.JS with NPM
- Python 3.7+

Project can be used with PyCharm or standalone.

In **PyCharm** Open project and setup Python venv with packages from requirements.txt

When using project without PyCharm:
1. Create venv `python3 -m venv <project-dir>/venv`
2. Run `source <project-dir>/venv/activate.bash` for linux or `<venv>\Scripts\Activate.ps1/bat` for Windows
3. Run `pip install -r requirements.txt --no-index`

**IMPORTANT:** Project requires selenium

Next setup Node.JS and NPM. Install it using your package manager or windows executable.
Make sure that npm is available in commandline.
Then proceed with:

1. Run `npm install -g vue-cli`
2. Go to `<project-dir>/webui` and run `npm install`
3. `npm install -g vega-lite vega-cli canvas selenium` This will install packages required for PNG/SVG charts rendering.

Now everything should be ready for testing.

### Executing
1. Run `app.py` in PyCharm or using this command: `python app.py`. Server should be running
2. Go to `<project-dir>/webui/` and run `npm run serve`

Flask server will be available at `127.0.0.1:5000` and web client at `127.0.0.1:8080`. The second address has to be used in browser. 
