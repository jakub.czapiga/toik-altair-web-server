import base64
import io
import sys

import altair as alt
from flask import Flask, request, jsonify
from flask_cors import CORS
import altair_saver

DEBUG = True
app = Flask(__name__, )
app.config.from_object(__name__)

# enable CORS
CORS(app, resources={r'/*': {'origins': '*'}})


class ValidationException(Exception):
    def __init__(self, data={}):
        self.data = data


def get_error_response(msg):
    return {"status": "error", "message": msg}


def validate_data_or_throw(data):
    if data is None:
        raise ValidationException(
            get_error_response("Invalid data")
        )

    if "data" not in data:
        raise ValidationException(
            get_error_response("No chart data in input data")
        )

    if 'format' in data['data']:
        if data['data']['format'] not in ['html', 'json', 'png', 'svg']:
            raise ValidationException(
                get_error_response("Invalid output format provided")
            )


@app.route('/chart', methods=['POST'])
def chart():
    print('Received:', request.get_data(), file=sys.stdout)
    input_data = request.get_json()

    try:
        validate_data_or_throw(input_data)
    except ValidationException as e:
        return jsonify(e.data)

    input_data = input_data['data']

    output_format = input_data.get('format', 'html')

    if output_format == 'png':
        mem = io.BytesIO()
    else:
        mem = io.StringIO()

    try:
        altair_saver.save(alt.Chart.from_dict(input_data["vega"]), fp=mem, fmt=output_format)
    except Exception as e:
        return jsonify(get_error_response(str(e)))

    mem.seek(0)

    if output_format == 'png':
        output_data = 'data:image/png;base64,{0}'.format(base64.b64encode(mem.read()).decode('ascii'))
    elif output_format == 'html':
        output_data = 'data:text/octet-stream;charset=utf-8;base64,{0}'.format(base64.b64encode(mem.read().encode('utf-8')).decode('ascii'))
    else:
        output_data = mem.read()

    return jsonify({
        "status": "success",
        "format": output_format,
        "data": output_data
    })


if __name__ == '__main__':
    app.debug = True
    app.run()
